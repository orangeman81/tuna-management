import { BehaviorSubject } from "rxjs";
import { distinctUntilChanged, pluck, share } from "rxjs/operators";

export class Store {
  constructor(state) {
    this.store$ = new BehaviorSubject(state);
  }

  get $state() {
    return this.store$.asObservable().pipe(share());
  }
  get state() {
    return this.store$.getValue();
  }
  set state(value) {
    this.store$.next(value);
  }

  push(prop, val) {
    this.state = {
      ...this.state,
      [prop]: val,
    };
  }

  pull(prop) {
    return this.state[prop];
  }

  $select(prop) {
    return this.$state.pipe(pluck(prop), distinctUntilChanged());
  }
}
