import { ajax } from "rxjs/ajax";
import { first, map } from "rxjs/operators";

export class ApiService {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }

  $find(path) {
    const headers = { Authorization: `${localStorage.getItem("token")}` };
    const $request = ajax({
      url: this.baseUrl + path,
      headers,
    }).pipe(
      first(),
      map((res) => res.response)
    );

    return $request;
  }

  async use(verb, path, payload) {
    const url = this.baseUrl + path;
    const headers = { Authorization: `${localStorage.getItem("token")}` };
    switch (verb) {
      case "GET": {
        const $get = await fetch(url, {
          headers,
        }).catch((err) => err);
        const $results = await $get.json();
        return $results;
      }
      case "POST": {
        const $post = await fetch(url, {
          method: "POST",
          body: JSON.stringify(payload),
          headers,
        }).catch((err) => err);
        const $results = await $post.json();
        return $results;
      }
    }
  }

  async composeDetails(userId, id) {
    const details = await this.use(
      "GET",
      `dettaglio-lavorazione-contratto.php?id_user=${userId}&id_contratto=${id}`
    );

    const results = await details["data"];

    if (results.length > 0) {
      const select = await this.use(
        "GET",
        `seleziona-fase.php?id_tipo_servizio=${details["data"][0]["id_tipo_servizio"]}&id_fase=${details["data"][0]["id_fase"]}`
      );
      return {
        results,
        select,
      };
    } else {
      throw new Error("Dettaglio contratto non disponibile");
    }
  }
}
