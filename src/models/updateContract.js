export class UpdateContractDTO {
  constructor(contract, userId, contractId) {
    this.specifiche = contract["specifiche_risorse"];
    this.dt_inizio = this.parseDate(contract["data_inizio_effettiva"]);
    this.dt_fine = this.parseDate(contract["data_fine_effettiva"]);
    this.fase = contract["id_status_fase"];
    this.lettura = contract["stato_lettura"];
    this.note = contract["proposte"];
    this.criticita = contract["criticita"];
    this.id_user = userId;
    this.id_contratto = contractId;
  }

  parseDate(date) {
    const parsedDate = new Date(date);
    const actualDate = `${parsedDate.getDate() +
      "-" +
      (parsedDate.getMonth() + 1) +
      "-" +
      parsedDate.getFullYear()}`;

    return actualDate;
  }
}
