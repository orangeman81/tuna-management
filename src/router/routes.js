import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Error from "../views/Error.vue";

export const routes = [
  {
    path: "/",
    name: "Contratti",
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/error",
    name: "Error",
    component: Error,
  },
  {
    path: "/plants",
    name: "Impianti",
    component: () =>
      import(/* webpackChunkName: "plants" */ "../views/Plants.vue"),
  },
  {
    path: "/headquarters",
    name: "Sedi operative",
    component: () =>
      import(
        /* webpackChunkName: "headquarters" */ "../views/Headquarters.vue"
      ),
  },
  {
    path: "/registry",
    name: "Anagrafiche",
    component: () =>
      import(/* webpackChunkName: "registry" */ "../views/Registry.vue"),
  },
  {
    path: "/edit/:id",
    name: "Edit",
    component: () =>
      import(/* webpackChunkName: "registry" */ "../views/Edit.vue"),
    props: true,
  },
];
